package tehran.basic.layouts;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import tehran.basic.R;

public class RelativeBasicActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relative_basic);
    }
}
